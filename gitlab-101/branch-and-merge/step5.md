# Let’s add our new script to the stage, commit, and push our branch

## Task

`git add hello.rb`{{execute}}  

`git commit -m 'adding in hello world script'`{{execute}}  

`git push origin add-hello-world`{{execute}}  

Origin has our new branch, this is how it looks in GitLab.  
![Branches](./assets/mergerequest_5.png)
